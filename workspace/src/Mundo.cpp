// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//  open y mkfifo
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// read
#include <unistd.h>

// mmap
#include <sys/mman.h>

#define MAX_PTS 1

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close(fd_logger);
	unlink(fichero); // Fichero proyectado en memoria, también se puede poner al final del código del bot, después de munmap(punt_dat, s.st_size);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	
	// Las 2 próximas líneas son para que las raquetas se paren de mover al levantar la tecla
	//jugador1.velocidad.y=0;
	//jugador2.velocidad.y=0;
	
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		char salida[100];
		sprintf(salida, "El jugador 2 ha marcado un punto, lleva un total de %d punto(s)\n", puntos2);
		write(fd_logger, salida, strlen(salida));
		
		if(puntos2 >= MAX_PTS) exit(1);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		char salida[100];
		sprintf(salida, "El jugador 1 ha marcado un punto, lleva un total de %d punto(s)\n", puntos1);
		write(fd_logger, salida, strlen(salida));
		if(puntos1 >= MAX_PTS) exit(1);
	}
	puntdatos->esfera=esfera;
	puntdatos->raqueta1=jugador1;

	if(puntdatos->accion==-1) OnKeyboardDown('s', 0, 0);
	if(puntdatos->accion==1) OnKeyboardDown('w', 0, 0);
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	// Teclas del jugador 1
	case 'a':jugador2.velocidad.y=0;break; // Parar raqueta enemigo del jugador 1 (jugador 2)
	case 'd': // Reducir tamaño enemigo del jugador 1 (jugador 2)
		{
			if (jugador2.i==1) // Grande, voy a disminuir (cota 1 es menor que cota 2)
			{
				jugador2.y1=jugador2.y1+0.3;
				jugador2.y2=jugador2.y2-0.3;
				jugador2.i*=(-1);
			}
			else if (jugador2.i==-1) // Pequeño, voy a aumentar
			{
				jugador2.y1=jugador2.y1-0.3;
				jugador2.y2=jugador2.y2+0.3;
				jugador2.i*=(-1);
			}
			break;
		}
	case 's':jugador1.velocidad.y=-4;break; // Mover jugador 1
	case 'w':jugador1.velocidad.y=4;break; // Mover jugador 1
	
	
	// Teclas del jugador 2
	case 'j':jugador1.velocidad.y=0;break; // Parar raqueta enemigo del jugador 2 (jugador 1)
	case 'l': // Reducir tamaño enemigo del jugador 2 (jugador 1)
		{
			if (jugador1.i==1) // Grande, voy a disminuir (cota 1 es menor que cota 2)
			{
				jugador1.y1=jugador1.y1+0.3;
				jugador1.y2=jugador1.y2-0.3;
				jugador1.i*=(-1);
			}
			else if (jugador1.i==-1) // Pequeño, voy a aumentar
			{
				jugador1.y1=jugador1.y1-0.3;
				jugador1.y2=jugador1.y2+0.3;
				jugador1.i*=(-1);
			}
			break;
		}
	case 'k':jugador2.velocidad.y=-4;break; // Mover jugador 2
	case 'i':jugador2.velocidad.y=4;break; // Mover jugador 2
	}
}

void CMundo::Init()
{
	if(0 != system("pidof -x logger > /dev/null")) // Si comienza a ejecutarse el proceso tenis sin que se esté ejecutando
	{ 					       // antes el proceso logger -> error
		printf("TENIS: logger not executing before tenis\n"); 
		exit(1);
	}
	
	fd_logger=open(tuberia_logger, O_WRONLY); // Tubería de tenis.cpp a logger.cpp (creada por logger y abierta aquí en la clase Mundo de tenis.cpp)
	if(fd_logger < 0)
	{
                printf("TENIS: Error abriendo tuberia_logger\n");
                exit(1);
        }
	else
	{
		printf("TENIS: tuberia_logger abierta correctamente\n");
		puntdatos=&datos;
		
		// IMPORTANTE: no puedo hacer mmap con fichero creado con mkfifo
		
		/*int i_mkfifo = mkfifo(fichero, 0777); // tenis es el encargado de crear el fichero a proyectar
		if (i_mkfifo < 0)
		{
			printf("TENIS: Error creando el fichero\n");
			perror("mkfifo() error");
			exit(1);
		}
		else printf("TENIS: fichero creado correctamente\n");*/
	
	
		umask(0000); // Para deactivar mascara por defecto 0002 y crear los ficheros con los permisos que doy por código
		// unlink(fichero); // Protección por si ya existe // No hace falta porque si existe lo trunca
		f_datosmem=open(fichero, O_RDWR|O_CREAT|O_TRUNC, 0777);
		
		if(f_datosmem < 0)
		{
			printf("TENIS: Error creando o abriendo el fichero\n");
			exit(1);
		}
		else
		{
			printf("TENIS: Fichero creado y abierto correctamente\n");
			write(f_datosmem, &datos, sizeof(DatosMemCompartida));
			struct stat s;
			fstat(f_datosmem, &s);
			puntdatos=(DatosMemCompartida *)mmap(NULL, s.st_size, PROT_WRITE|PROT_READ, MAP_SHARED, f_datosmem, 0);
			if(puntdatos == MAP_FAILED)
	        	{
				printf("TENIS: Error proyectando el fichero\n");
				perror("mmap() error");
			}
			else printf("TENIS: Fichero proyectado correctamente\n");
			close(f_datosmem);			
	
			Plano p;
			//pared inferior
			p.x1=-7;p.y1=-5;
			p.x2=7;p.y2=-5;
			paredes.push_back(p);

			//superior
			p.x1=-7;p.y1=5;
			p.x2=7;p.y2=5;
			paredes.push_back(p);

			fondo_izq.r=0;
			fondo_izq.x1=-7;fondo_izq.y1=-5;
			fondo_izq.x2=-7;fondo_izq.y2=5;

			fondo_dcho.r=0;
			fondo_dcho.x1=7;fondo_dcho.y1=-5;
			fondo_dcho.x2=7;fondo_dcho.y2=5;

			//a la izq
			jugador1.g=0;
			jugador1.x1=-6;jugador1.y1=-1;
			jugador1.x2=-6;jugador1.y2=1;

			//a la dcha
			jugador2.g=0;
			jugador2.x1=6;jugador2.y1=-1;
			jugador2.x2=6;jugador2.y2=1;
		}
	}
}
