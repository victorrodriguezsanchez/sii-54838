#include <stdlib.h>
#include <stdio.h>

//  open, stat, fstat
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// read, close usleep
#include <unistd.h>

// mmap
#include <sys/mman.h>

#include "DatosMemCompartida.h"

int main(int argc, char *argv[]){
	int fd_fichero;
	char *fichero= "/tmp/fichero";
	DatosMemCompartida *punt_dat;
	
	if(0 != system("pidof -x logger > /dev/null")){ 	      // Si comienza a ejecutarse el proceso bot sin que se esté ejecutando
		printf("BOT: logger not executing before bot\n");     // antes el proceso logger -> error
		return 1;
	}
	
	if(0 != system("pidof -x tenis > /dev/null")){ 	              // Si comienza a ejecutarse el proceso bot sin que se esté ejecutando
		printf("BOT: tenis not executing before bot\n");      // antes el proceso tenis -> error
		return 1;
	}
	
	fd_fichero = open(fichero, O_RDWR);
	if(fd_fichero < 0){
		printf("BOT: Error abriendo el fichero\n");
		return 1;
	}
	else
	{
		printf("BOT: Fichero abierto correctamente\n");
		struct stat s;
		fstat(fd_fichero, &s);
		punt_dat = (DatosMemCompartida *)mmap(NULL, s.st_size, PROT_WRITE|PROT_READ, MAP_SHARED, fd_fichero, 0);
	        if(punt_dat == MAP_FAILED)
	        {
			printf("BOT: Error proyectando el fichero\n");
		}
		else printf("BOT: Fichero proyectado correctamente\n\n");
	        close(fd_fichero);

		while(1)
		{
			usleep(25000); // Espera 25 ms
			
			// Lógica movimiento
			if(punt_dat->esfera.centro.y > (punt_dat->raqueta1.y1+punt_dat->raqueta1.y2)/2.0) // Esfera por encima del jugador
				punt_dat->accion=1; // Subir
			else if(punt_dat->esfera.centro.y < (punt_dat->raqueta1.y1+punt_dat->raqueta1.y2)/2.0) // Esfera por debajo del jugador
      	                	punt_dat->accion=-1; // Bajar
			else punt_dat->accion=0; // Nada
				
			// Finalización bucle
			if(0 != system("pidof -x tenis > /dev/null"))
			{
				printf("\033[33m" "BOT: Ha terminado tenis, por lo que termina bot\n" "\033[0m");
				break; // Si termina de ejecutarse proceso tenis, termina bot
			}
			if(0 != system("pidof -x logger > /dev/null"))
			{
				printf("\033[33m" "BOT: Ha terminado logger, por lo que termina bot\n" "\033[0m");
				break; // Si termina de ejecutarse proceso logger, termina bot
			}
		}
		munmap(punt_dat, s.st_size);
		return 0;
	}
}
