#include <stdlib.h>
#include <stdio.h>

//  open y mkfifo
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// read
#include <unistd.h>

int main(int argc, char *argv[]){
	char *tuberia_logger= "/tmp/tuberia_logger";
	char salida[100];
	
	umask(0000); // Para deactivar mascara por defecto 0002 y crear los ficheros con los permisos que doy por código
	unlink(tuberia_logger); // Proteger si quedó creada anteriormente
	int i_mkfifo = mkfifo(tuberia_logger, 0777); // logger es el encargado de crear la tubería tuberia_logger
	if (i_mkfifo < 0)
	{
		printf("LOGGER: Error creando tuberia_logger\n");
		return 1;
	}
	else printf("\nLOGGER: tuberia_logger creada correctamente\n");
	
	int fd_logger=open(tuberia_logger, O_RDONLY);
	if(fd_logger < 0)
	{
                printf("LOGGER: Error abriendo tuberia_logger\n");
                return 1;
        }
	else
	{
		printf("LOGGER: tuberia_logger abierta correctamente\n\n");
		int i_read;
		while(1)
		{
			i_read = read(fd_logger, salida, 100);
			if(i_read == -1) printf("LOGGER: Error leyendo tuberia_logger\n");
			else
			{
				//printf("LOGGER: Lectura correcta tuberia_logger\n");
				printf("%s", salida);
			}
			
			// Finalización bucle
			if(0 != system("pidof -x tenis > /dev/null"))
			{
				printf("\033[33m" "LOGGER: Ha terminado tenis, por lo que termina logger\n" "\033[0m");
				break;
			}
			if(0 != system("pidof -x bot > /dev/null"))
			{
				printf("\033[33m" "LOGGER: Ha terminado bot, por lo que termina logger\n" "\033[0m");
				break;
			}
		}
		close(fd_logger);
		unlink(tuberia_logger);
		return 0;
	}
}
